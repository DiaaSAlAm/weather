//
//  DaysForecastViewController.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import UIKit

class DaysForecastViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var daysForecastTableView: UITableView!
    @IBOutlet private weak var cityTableView: UITableView!
    @IBOutlet private weak var cityTableViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var searchButton: UIButton!
    @IBOutlet private weak var cityNameLb: UILabel!
    
    //MARK: - Properties
    var viewModel: DaysForecastViewModelProtocol! = DaysForecastViewModel()
    private let cellIdentifier = CellIdentifiers.commonCell
    private let daysCellIdentifier = CellIdentifiers.daysForecastCell
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        searchButton.roundCorners([.topRight, .bottomRight], radius: 8)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyboard_CityTableView()
    }
    
    static func create(with selectedCitiesModel: Observable<MainCitiesModel?>) -> DaysForecastViewController {
        let view = DaysForecastViewController.instantiateViewController(StoryboardNames.main)
        view.viewModel.navigateSelectedCitiesModel = selectedCitiesModel.value
        view.updateModel { selectedCitiesModel.value = $0 }
        return view
    }
    
   private func updateModel(completion: @escaping (MainCitiesModel?) -> Void){
        self.viewModel.newSelectedCitiesModel.observe(on: self) { completion( $0)}
    }
     
    //MARK: - IBOutlets
    @IBAction
    func didTappedSearch(_ sender: UIButton) {
        guard let q = searchTextField.text else {return}
        dismissKeyboard_CityTableView()
        viewModel.didTappedSerch(q: q)
    }
}

// MARK: - Func
extension DaysForecastViewController: UIGestureRecognizerDelegate {
    
    private func setViewDidLoad(){
        registerTableView()
        addUITapGestureRecognizer()
        bind(to: viewModel)
    }
    private func bind(to viewModel: DaysForecastViewModelProtocol) {
        viewModel.viewDidLoad()
        viewModel.selectedCitiesModel.observe(on: self) { [weak self] (city) in
            self?.cityNameLb.text = city?.city
            self?.daysForecastTableView.reloadData()
        }
        viewModel.searchModel.observe(on: self) { [weak self] _ in
            self?.updateCityTableView()
        }
        viewModel.loading.observe(on: self) { [weak self] (loading) in
            switch loading {
            case .show:
                self?.showLoadingIndicator()
            case .hidde:
                self?.hideLoadingIndicator()
            }
        }
        
        
    }
    
    //MARK: - Register  CollectionView Cell
    private func registerTableView(){
        cityTableView.tableFooterView = UIView()
        daysForecastTableView.tableFooterView = UIView()
        daysForecastTableView.allowsSelection = false
        cityTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        daysForecastTableView.register(UINib(nibName: daysCellIdentifier, bundle: nil), forCellReuseIdentifier: daysCellIdentifier)
    }
    
    private func updateCityTableView(){
        UIView.animate(withDuration: 0.3) {
            self.cityTableView.fitHeightToContent(self.view, self.cityTableViewHeight, 200)
        }
    }
    
    private func closeCityTableView(){
        UIView.animate(withDuration: 0.3) { 
            self.cityTableViewHeight.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - add Tap Gesture handle tapping empty space (not a cell)
    private func addUITapGestureRecognizer(){
        //for single or multiple taps.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.delegate = self
        self.daysForecastTableView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        dismissKeyboard_CityTableView()
    }
    
    private func dismissKeyboard_CityTableView(){
        view.endEditing(true)
        closeCityTableView()
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // only handle tapping empty space (not a cell)
        let point = gestureRecognizer.location(in: daysForecastTableView)
        let indexPath = daysForecastTableView.indexPathForRow(at: point)
        return indexPath == nil
    }
}
 

// MARK: - UITableView Data Source _ Delegate
extension DaysForecastViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == cityTableView {
            dismissKeyboard_CityTableView()
            viewModel.didSelectSearchCity(indexPath)
            viewModel.getDaysForecast()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case daysForecastTableView:
            let count = viewModel.selectedCitiesModel.value?.daily?.count ?? 0
            return count 
        default:
            return viewModel.searchModel.value?.cityList?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case daysForecastTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: daysCellIdentifier) as! DaysForecastCell
            viewModel.daysConfigureCell(cell: cell, indexPath: indexPath)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CommonCell
            viewModel.cityConfigureCell(cell: cell, indexPath: indexPath)
            return cell
            
        }
        
    }
}

 

