//
//  DaysForecastCell.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/8/21.
//

import UIKit

class DaysForecastCell: UITableViewCell, DaysForecastCellProtocol {

    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var minMaxCelsiusLabel: UILabel!
    @IBOutlet private weak var iconImage: UIImageView!
    @IBOutlet private weak var weatherDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(setModel: DaysForecastSetModel) {
        dateLabel.text = setModel.date
        minMaxCelsiusLabel.text = setModel.celsius
        iconImage.loadImageAsync(with: setModel.icon)
        weatherDescriptionLabel.text = setModel.weatherDescription
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
