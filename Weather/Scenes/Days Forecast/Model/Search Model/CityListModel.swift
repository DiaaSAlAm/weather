//
//  CityListModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import Foundation

// MARK: - List
struct CityListModel: Codable {
    let id: Int?
    var name: String?
    var coord: Coord?
    let main: Main?
    let dt: Double?
    let wind: Wind?
    let sys: Sys?
    let clouds: Clouds?
    let weather: [Weather]?
    
    mutating func setCurrentLocation_Name(_ name: String?, _ coord: Coord?){
        self.name = name
        self.coord = coord
    }
    
    init(_ id: Int? = nil,name: String?,coord: Coord?,_ main: Main? = nil,dt: Double? = nil, wind: Wind? = nil,_ sys: Sys? = nil, clouds: Clouds? = nil,_ weather: [Weather]? = nil) {
        self.id = id
        self.name = name
        self.coord = coord
        self.main = main
        self.dt = dt
        self.wind = wind
        self.sys = sys
        self.clouds = clouds
        self.weather = weather
    }
    
     
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Int
}

// MARK: - Coord
struct Coord: Codable {
    var lat, lon: Double?
    
    mutating func setCurrentLocation(_ lat: Double, _ lon: Double){
        self.lat = lat
        self.lon = lon
    }
}

// MARK: - Main
struct Main: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, humidity: Int
    let seaLevel, grndLevel: Int?

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
    }
}

// MARK: - Sys
struct Sys: Codable {
    let country: String
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

// MARK: - Wind
struct Wind: Codable {
    let speed: Double
    let deg: Int
}
