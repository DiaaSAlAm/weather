//
//  SearchCityListEntity+Mapping.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/17/21.
//

import Foundation
import CoreData

extension Search {
    func toDTO() -> SearchModel {
        return .init(q: q, cityList: cityList?.allObjects.map{($0 as! CityList).toDTO()})
    }
    
    func toEntity(in context: NSManagedObjectContext, model: SearchModel?) {
        q = model?.q
        model?.cityList?.forEach {
            addToCityList($0.toEntity(in: context))
        }
    }
}


extension SearchModel {
    func toEntity(in context: NSManagedObjectContext) -> Search {
        let entity: Search = .init(context: context)
        entity.q = q
        cityList?.forEach{
            entity.addToCityList($0.toEntity(in: context))
        }
        return entity
    }
}

extension CityList {
    func toDTO() -> SearchCityListModel {
        return .init(id: Int(id), city: city, icon: icon, weatherDescription: weatherDescription, tempMin: tempMin, tempMax: tempMax, lat: lat, lon: lon, dt: dt)
    }
}

extension SearchCityListModel {
    
    func toEntity(in context: NSManagedObjectContext) -> CityList {
        let entity: CityList = .init(context: context)
        entity.id = Int64(id)
        entity.city = city
        entity.icon = icon
        entity.weatherDescription = weatherDescription
        entity.tempMin = tempMin ?? 0
        entity.tempMax = tempMax ?? 0
        entity.lat = lat ?? 0
        entity.lon = lon ?? 0
        entity.dt = dt ?? 0
        return entity
    }
}

extension CityListModel {
    
    func toEntity(in context: NSManagedObjectContext) -> CityList {
        let entity: CityList = .init(context: context)
        entity.id = Int64(id ?? 0)
        entity.city = (name ?? "") + "," + (sys?.country ?? "")
        entity.icon = weather?.first?.icon ?? ""
        entity.weatherDescription = weather?.first?.weatherDescription ?? ""
        entity.tempMin = main?.tempMin ?? 0
        entity.tempMax = main?.tempMax ?? 0
        entity.lat = coord?.lat ?? 0
        entity.lon = coord?.lon ?? 0
        entity.dt = dt ?? 0
        return entity
    } 
}
