//
//  SearchModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/17/21.
//

import Foundation

class SearchModel: Codable {
    var q: String?
    var cityList: [SearchCityListModel]?
    init(q: String?, cityList: [SearchCityListModel]?) {
        self.q = q
        self.cityList = cityList
    }
}

class SearchCityListModel: Codable {
    var id: Int
    var city: String?
    var icon, weatherDescription: String?
    var tempMin, tempMax: Double?
    var lat, lon: Double?
    var dt: Double?
    
    init(id: Int, city: String? = nil,icon: String? = nil, weatherDescription: String? = nil,tempMin: Double? = nil, tempMax: Double? = nil, lat: Double? = nil, lon: Double? = nil, dt: Double? = nil) {
        self.id = id
        self.city = city
        self.icon = icon
        self.weatherDescription = weatherDescription
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.lat = lat
        self.lon = lon
        self.dt = dt
    }
}
