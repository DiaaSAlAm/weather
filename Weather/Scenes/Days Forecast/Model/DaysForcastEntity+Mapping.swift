//
//  DaysForcastEntity+Mapping.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/17/21.
//

import Foundation
import CoreData

extension DaysForecast {
    func toDTO() -> DaysForecastMappingModel {
        return .init(dt: dt, tempMin: tempMin, tempMax: tempMax, icon: icon,weatherDescription: weatherDescription)
    }
}

extension Daily {
    func toEntity(in context: NSManagedObjectContext) -> DaysForecast {
        let entity: DaysForecast = .init(context: context)
        entity.dt = dt
        entity.tempMax = temp.max ?? 0
        entity.tempMin = temp.min ?? 0
        entity.icon = weather.getElement(at: 0)?.icon
        entity.weatherDescription = weather.getElement(at: 0)?.weatherDescription
        return entity
    }
}

extension DaysForecastMappingModel {
    func toEntity(in context: NSManagedObjectContext) -> DaysForecast {
        let entity: DaysForecast = .init(context: context)
        entity.dt = dt
        entity.tempMax = tempMax
        entity.tempMin = tempMin
        entity.icon = icon
        entity.weatherDescription = weatherDescription
        return entity
    }
}
