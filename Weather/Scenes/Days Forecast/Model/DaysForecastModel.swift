//
//  DaysForecastModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/8/21.
//

import Foundation

// MARK: - DaysForecastModel
struct DaysForecastModel: Codable {
    let lat, lon: Double
    let daily: [Daily]

    enum CodingKeys: String, CodingKey {
        case lat, lon, daily
    }
}

// MARK: - Daily
struct Daily: Codable {
    let dt : Double
    let temp: Temp
    let weather: [Weather]

    enum CodingKeys: String, CodingKey {
        case dt, temp, weather
    }
}

// MARK: - Temp
struct Temp: Codable {
    var day, min, max, night: Double?
    let eve, morn: Double
}
     
