//
//  DaysForecastMappingModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/25/21.
//

import Foundation

struct DaysForecastMappingModel: Codable {
    var dt: Double
    let tempMin: Double
    let tempMax: Double
    let icon: String
    let weatherDescription: String

    init(dt: Double? = nil,tempMin: Double? = nil,tempMax: Double? = nil, icon: String? = nil,weatherDescription: String? = nil){
        self.dt = dt ?? 0
        self.tempMin = tempMin ?? 0
        self.tempMax = tempMax ?? 0
        self.icon = icon ?? ""
        self.weatherDescription = weatherDescription ?? ""
    }
}

struct DaysForecastSetModel {
    var date: String?
    var celsius: String?
    var icon: String?
    var weatherDescription: String?
    
    init(model: DaysForecastMappingModel){
        let celsiusMin = model.tempMin.convertTemp(from: .kelvin, to: .celsius)
        let celsiusMax = model.tempMax.convertTemp(from: .kelvin, to: .celsius)
        let fullCelsius = "\(celsiusMin) / \(celsiusMax)"
        self.celsius = fullCelsius
        self.date = Date(timeIntervalSince1970: TimeInterval(model.dt)).convertDateToString(dateFormat: Constants.dateFormatterE_d_M)
        self.icon = Environment.imageURL.absoluteString + (model.icon) + Constants.image2xPng
        self.weatherDescription = model.weatherDescription
    }
}
