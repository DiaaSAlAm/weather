//
//  DaysForecastProtocol.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/12/21.
//

import Foundation


enum Loading {
     case show , hidde
 }

protocol DaysForecastModelInput {
    func viewDidLoad()  
    func didSelectSearchCity(_ indexPath: IndexPath) 
    func cityConfigureCell(cell: CommonCellProtocol, indexPath: IndexPath)
    func daysConfigureCell(cell: DaysForecastCellProtocol, indexPath: IndexPath)
    func getDaysForecast()
    func didTappedSerch(q: String)
}

protocol DaysForecastViewModelOutput {
    var navigateSelectedCitiesModel: MainCitiesModel? { get set }
    var selectedCitiesModel: Observable<MainCitiesModel?> { get }
    var newSelectedCitiesModel: Observable<MainCitiesModel?> { get set } 
    var searchModel: Observable<SearchModel?> {get}
    var loading: Observable<Loading> {get}
}

protocol DaysForecastCellProtocol { // it's will call when register cell
    func configureCell(setModel: DaysForecastSetModel)
}

protocol DaysForecastViewModelProtocol: DaysForecastModelInput, DaysForecastViewModelOutput {}

