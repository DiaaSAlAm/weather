//
//  CityListCoreData.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/18/21.
//

import Foundation

protocol CityListCoreDataProtocol {
    func retrieveSearchRecords(q: String,searchModel: SearchModel?,cityListModel:[CityListModel],completion: @escaping (SearchModel?) -> Void)
    func getSearchRecordByQ(q: String,completion: @escaping (SearchModel?) -> Void)
}

final class CityListCoreData: CityListCoreDataProtocol {
    
    private let searchEntityName = CoreDataEntityName.search
    private let coreDataStorage: CoreDataStorageProtocol? = CoreDataStorage()
    
    func retrieveSearchRecords(q: String,searchModel: SearchModel?,cityListModel:[CityListModel],completion: @escaping (SearchModel?) -> Void){
        updateSearchRecordByQ(q: q, searchModel: searchModel, cityListModel: cityListModel) { [weak self] (success) in
            switch success {
            case true:
                self?.getSearchRecordByQ(q: q, completion: completion)
            case false:
                self?.createNewSearchRecords(q: q, cityListModel: cityListModel, completion: { [weak self] in
                    self?.getSearchRecordByQ(q: q, completion: completion)
                })
            }
        }
    }
    
    func getSearchRecordByQ(q: String,completion: @escaping (SearchModel?) -> Void){
        self.coreDataStorage?.getRecordByKeyValue(entityClass: Search.self, with: self.searchEntityName, keyValue: ["q": q], completion: { [weak self] (result) in
                switch result {
                case .success(let model):
                    completion(model?.toDTO())
                case .failure(let error):
                    self?.showMessage(message: error.localizedDescription, messageKind: .error)
                }
            })
    }
    
    private func updateSearchRecordByQ(q:String,searchModel: SearchModel?,cityListModel:[CityListModel],completion: @escaping (Bool) -> Void){
        self.coreDataStorage?.updateRecordByKeyValue(entityClass: Search.self, entityName: self.searchEntityName, keyValue: ["q":q], completion: { [weak self] (result) in
            switch result{
            case .success(let model,let context):
                guard model != nil else {
                    completion(false)
                    return
                }
                model?.toEntity(in: context, model: searchModel)
                model?.removeFromCityList(model?.cityList ?? NSSet())
                cityListModel.forEach{
                    model?.addToCityList($0.toEntity(in: context))
                }
                try? context.save()
                completion(true)
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
                completion(false)
            }
        })
    }
    
    private func createNewSearchRecords(q: String,cityListModel:[CityListModel],completion: @escaping () -> Void){
        self.coreDataStorage?.createNewRecords(entityClass: Search.self, entityName: self.searchEntityName, completion: { [weak self] (result) in
            switch result{
            case .success(let model,let context):
                model?.setValue(q, forKey: "q")
                cityListModel.forEach{
                    model?.addToCityList($0.toEntity(in: context))
                }
                try? context.save()
                completion()
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
            }
        })
    }
    
    private func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
}

