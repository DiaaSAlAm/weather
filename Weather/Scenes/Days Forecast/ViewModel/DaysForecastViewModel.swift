//
//  DaysForecastViewModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/12/21.
//

import Foundation
import CoreData

final class DaysForecastViewModel: BaseAPI<MainNetwork>, DaysForecastViewModelProtocol {
        
    
    //MARK: - Properties, OUTPUT 
    var navigateSelectedCitiesModel: MainCitiesModel?
    var selectedCitiesModel: Observable<MainCitiesModel?> = Observable(nil)
    var newSelectedCitiesModel: Observable<MainCitiesModel?> = Observable(nil)
    var searchModel: Observable<SearchModel?> = Observable(nil)
    
    
    var cityListCoreData: CityListCoreDataProtocol = CityListCoreData()
    var daysForecastCoreData: DaysForecastCoreDataProtocol = DaysForecastCoreData()
    var network: DaysForecastNetworkProtocol = DaysForecastNetwork()
    var cityId: Int? { return selectedCitiesModel.value?.id ?? 0 }
    var loading: Observable<Loading> = Observable(.hidde)
    
    //MARK: - Life Cycle, INPUT
    func viewDidLoad() {
        selectedCitiesModel.value = navigateSelectedCitiesModel
        getDaysForecast()
    } 
    
    func cityConfigureCell(cell: CommonCellProtocol, indexPath: IndexPath) {
        guard let model = searchModel.value?.cityList?.getElement(at: indexPath.row)else {return}
        let viewModel = CurrentCitySetModel(city: model.city, icon: model.icon ?? "", tempMin: model.tempMin ?? 0, tempMax: model.tempMax, lat: model.lat ?? 0, lon: model.lon ?? 0, dt: model.dt)
        cell.configureCell(setModel: viewModel)
    }
    
    func daysConfigureCell(cell: DaysForecastCellProtocol, indexPath: IndexPath) {
        guard let model = selectedCitiesModel.value?.daily?.getElement(at: indexPath.row)else {return}
        let setModel = DaysForecastSetModel(model: model)
        cell.configureCell(setModel: setModel)
    }
    
    func didSelectSearchCity(_ indexPath: IndexPath) {
        guard let model = searchModel.value?.cityList?.getElement(at: indexPath.row) else {return}
        let setModel = MainCitiesModel(id: model.id , city: model.city, icon: model.icon ?? "", tempMin: model.tempMin ?? 0, tempMax: model.tempMax, lat: model.lat ?? 0, lon: model.lon ?? 0, dt: model.dt, daily: [])
        newSelectedCitiesModel.value = setModel
        selectedCitiesModel.value = setModel
    }
    
    func getDaysForecast() {
        guard let lat = selectedCitiesModel.value?.lat else {return}
        guard let lon = selectedCitiesModel.value?.lon else { return }
        loading.value = .show
        network.getDaysForecast(lat: lat, lon: lon) {[weak self] (daily) in 
            guard daily.count != 0 else {
                self?.retrieveDaysForecastRecords()
                return
            }
            self?.daysForecastCoreData.addChildToParentRecordById(id: self?.cityId ?? 0, daily: daily, completion: {(success) in
                guard success != false else {
                    self?.loading.value = .hidde
                    return
                }
                self?.retrieveDaysForecastRecords()
            })
        }
    }
    
   private func retrieveDaysForecastRecords(){
        self.daysForecastCoreData.getRecordById(id: cityId ?? 0, completion: { [weak self] (selectedCitiesModel) in
            defer {self?.loading.value = .hidde }
            self?.selectedCitiesModel.value = selectedCitiesModel
        })
    }
    
    func didTappedSerch(q: String){
        loading.value = .show
        network.findCities(q: q) { [weak self] (cityListModel) in
            self?.retrieveSearchRecords(q: q, cityListModel: cityListModel)
        }
    }
    
   private func retrieveSearchRecords(q: String,cityListModel: [CityListModel]){
        guard cityListModel.count != 0  else {
            self.cityListCoreData.getSearchRecordByQ(q: q) {[weak self] (newSearchModel) in
                defer {self?.loading.value = .hidde}
                self?.searchModel.value = newSearchModel
            }
            return
        }
        self.cityListCoreData.retrieveSearchRecords(q: q, searchModel: self.searchModel.value, cityListModel: cityListModel, completion: { [weak self] (searchModel) in
            defer {self?.loading.value = .hidde}
            self?.searchModel.value = searchModel
        })
    }
}
