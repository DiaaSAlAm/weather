//
//  DaysForecastCoreData.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/18/21.
//

import Foundation

protocol DaysForecastCoreDataProtocol {
    func addChildToParentRecordById(id: Int, daily: [Daily],completion: @escaping (Bool) -> Void)
    func getRecordById(id: Int,completion: @escaping (MainCitiesModel?) -> Void)
}

final class DaysForecastCoreData: DaysForecastCoreDataProtocol {
    
    private let entityName = CoreDataEntityName.cities
    private let coreDataStorage: CoreDataStorageProtocol? = CoreDataStorage()
    
    func addChildToParentRecordById(id: Int, daily: [Daily],completion: @escaping (Bool) -> Void){
        coreDataStorage?.addChildToParentRecordByKeyValue(entityClass: Cities.self, with: entityName, keyValue: ["id":id], completion: {[weak self] (result) in
            switch result{
            case .success(let model,let context):
                model?.removeFromDaysForecast(model?.daysForecast ?? NSSet())
                daily.forEach{
                    model?.addToDaysForecast($0.toEntity(in: context))
                }
                try? context.save()
            completion(true)
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
                completion(false)
            }
        })
    }

    func getRecordById(id: Int,completion: @escaping (MainCitiesModel?) -> Void){
        coreDataStorage?.getRecordByKeyValue(entityClass: Cities.self, with: entityName, keyValue: ["id":id], completion: { [weak self] (result) in
            switch result {
            case .success(let model):
                completion(model?.toDTO())
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
                completion(nil)
            }
        })
    }
    
    private func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
}

