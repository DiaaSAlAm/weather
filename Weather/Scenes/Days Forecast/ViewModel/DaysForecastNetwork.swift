//
//  DaysForecastNetwork.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/18/21.
//

import Foundation

protocol DaysForecastNetworkProtocol {
    func getDaysForecast(lat: Double,lon: Double, completion: @escaping ([Daily]) -> Void)
    func findCities(q: String,completion: @escaping ([CityListModel]) -> Void)
}

final class DaysForecastNetwork: BaseAPI<MainNetwork>, DaysForecastNetworkProtocol {
    
    func getDaysForecast(lat: Double,lon: Double, completion: @escaping ([Daily]) -> Void) {
        self.fetchData(target: .getDaysForecast(lat, lon), responseClass: DaysForecastModel.self) { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                guard let daily = response?.daily else {
                    self.showMessage(message: StaticMessages.genericErrorMessage, messageKind: .error)
                    completion([])
                    return
                }
                completion(daily)
            case .failure(let error):
                let err = error.localizedDescription
                print(err)
                self.showMessage(message: err, messageKind: .error)
                completion([])
            }
        }
    }
    
    func findCities(q: String,completion: @escaping ([CityListModel]) -> Void){
        self.fetchData(target: .findCities(q), responseClass: BaseResponse<[CityListModel]>.self) { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                guard let cityListModel = response?.list, cityListModel.count != 0 else  {
                    self.showMessage(message: StaticMessages.genericErrorMessage, messageKind: .error)
                    completion([])
                    return
                }
                completion(cityListModel)
            case .failure(let error):
                let err = error.localizedDescription
                print(err)
                self.showMessage(message: err, messageKind: .error)
                
            }
        }
    }
    
    private func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
    
}
