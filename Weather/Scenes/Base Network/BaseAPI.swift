//
//  BaseAPI.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import UIKit

class BaseAPI<T: TargetType> {
    
    private func createAndRetrieveURLSession() -> URLSession {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 15.0
        sessionConfiguration.timeoutIntervalForResource = 30.0
        return URLSession(configuration: sessionConfiguration)
    }
    
    func fetchData<M: Decodable>(target: T, responseClass: M.Type, completion:@escaping (Result<M?, Error>) -> Void) {
        
        let httpMethod =  target.method.rawValue
        let parameters =  buildParams(task: target.task).dictionary
        let httpBody = buildParams(task: target.task).data
        let headers =  target.headers
        let urlString = target.baseURL + target.path
        let url = buildURL(task: target.task, parameters: parameters ?? [:], urlString: urlString)
        var request = URLRequest(url: url)
        let nsError = NSError(domain: target.baseURL, code: 0, userInfo: [NSLocalizedDescriptionKey: StaticMessages.genericErrorMessage])
        request.httpMethod = httpMethod
        request.httpBody = httpBody
        request.allHTTPHeaderFields = headers
        
        let dataTask = createAndRetrieveURLSession().dataTask(with: request) { (data, response,error) in
            if error != nil  {
                // TODO: - Log to Crashlytics
                let err =  error as NSError?
                debugPrint("\(urlString) Unresolved error \(err ?? nsError), \(String(describing: (err)?.userInfo))")
                completion(.failure(nsError))
                return
            }
            do {
                let responseObj = try JSONDecoder().decode(M.self, from: data!)
                completion(.success(responseObj))
                return
            }catch let err {
                print(err)
                // TODO: - Log to Crashlytics
                debugPrint("\(urlString) Unresolved error \(err), \((err as NSError).userInfo)")
                completion(.failure(nsError))
                return
            }
        }
        dataTask.resume()
    }
    
    
    private func buildParams(task: Task) -> (dictionary: [String: Any]?,data: Data?) {
        switch task {
        case .requestPlain:
            return (nil,nil)
        case .requestParametersJSONEncoding(let parameters):
            return (nil, try? JSONSerialization.data(withJSONObject: parameters))
        case .requestParametersURLEncoding(let parameters):
            return (parameters, nil)
        }
    }
    
    private func buildURL(task: Task,parameters: [String: Any], urlString: String ) -> URL {
        switch task {
        case .requestParametersURLEncoding:
            var urlComponents = URLComponents(string: urlString)
            var queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                queryItems.append(URLQueryItem(name: key, value: "\(value)"))
            }
            urlComponents?.queryItems = queryItems
            return (urlComponents?.url!)!
        default:
            return URL(string: urlString)!
        }
    }
}
