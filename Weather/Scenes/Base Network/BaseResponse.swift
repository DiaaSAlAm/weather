//
//  BaseResponse.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import Foundation

class BaseResponse<T: Codable>: Codable {
    var message: String?
    var cod: String?
    var count: Int?
    var list: T?
    
}
