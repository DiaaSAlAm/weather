//
//  CoreDataEnumResults.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/16/21.
//

import Foundation
import CoreData

public enum Results<Success,Context, Failure> where Failure : Error, Context: NSManagedObjectContext {

    /// A success, storing a `Success` value.
    /// A success, storing a `Context` value.
    case success(Success, Context)

    /// A failure, storing a `Failure` value.
    case failure(Failure)
    
}
