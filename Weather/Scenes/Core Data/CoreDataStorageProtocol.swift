//
//  CoreDataStorageProtocol.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/13/21.
//

import Foundation
import CoreData

protocol CoreDataStorageProtocol {
    func createNewRecords<M>(entityClass: M.Type, entityName: String, completion: @escaping (Results<M?, NSManagedObjectContext, CoreDataStorageError>) -> Void) where M : NSFetchRequestResult
    func updateRecordByKeyValue<M>(entityClass: M.Type, entityName: String, keyValue: [String: Any], completion: @escaping (Results<M?, NSManagedObjectContext, CoreDataStorageError>) -> Void) where M : NSFetchRequestResult
    func retrieveRecords<M>(entityClass: M.Type,entityName: String,completion: @escaping ((Result<[M]?, CoreDataStorageError>)) -> Void) where  M: NSFetchRequestResult
    func getRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping (Result<M?, CoreDataStorageError>) -> Void) where M: NSFetchRequestResult
    func addChildToParentRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping (Results<M?, NSManagedObjectContext,CoreDataStorageError>) -> Void) where M: NSFetchRequestResult
    func deleteRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping ((Result<Bool?, CoreDataStorageError>)) -> Void) where M: NSManagedObject
    func deleteAllRecords(entityName: String)
}
