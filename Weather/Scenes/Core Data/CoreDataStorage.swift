//
//  CoreDataStorage.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/13/21.
//

import Foundation
import CoreData

final class CoreDataStorage: NSObject {
    
    private let coreData: CoreData
    
    init(coreData: CoreData = CoreData.shared) {
        self.coreData = coreData
    }
    
    // MARK: - Private - context delete all, context save
    private func contextExecute(by entityName: String,deleteALL: NSBatchDeleteRequest) {
        coreData.performBackgroundTask { context in
            do {
                try context.execute(deleteALL)
                print("Data deleted from Context")
            } catch {
                fatalError("Unresolved error \(error.localizedDescription)")
            }
        }
    }
    
    private func contextSave() {
        coreData.performBackgroundTask { context in
            do {
                try context.save()
                print("Data save from Context")
            } catch {
                fatalError("Unresolved error \(error.localizedDescription)")
            }
        }
    }
    
}

extension CoreDataStorage: CoreDataStorageProtocol {
    
    
    func createNewRecords<M>(entityClass: M.Type, entityName: String, completion: @escaping (Results<M?, NSManagedObjectContext, CoreDataStorageError>) -> Void) where M : NSFetchRequestResult {
        coreData.performBackgroundTask {(context) in
            let userEntity = NSEntityDescription.entity(forEntityName: entityName, in: context)!
            let cmsg = NSManagedObject(entity: userEntity, insertInto: context) as? M
            completion(.success(cmsg, context))
        }
    }
    
    
    func updateRecordByKeyValue<M>(entityClass: M.Type, entityName: String, keyValue: [String: Any], completion: @escaping (Results<M?, NSManagedObjectContext, CoreDataStorageError>) -> Void) where M : NSFetchRequestResult {
        coreData.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<M>(entityName: entityName)
            let key = keyValue.first?.key ?? ""
            let value = keyValue.first?.value
            let type = ((value as? String) != nil) ? "@" : "d"
            fetchRequest.predicate = NSPredicate(format: "\(key) == %\(type)", value as! CVarArg)
            do{
                let result = try context.fetch(fetchRequest)
                let firstResult = result.first
                completion(.success(firstResult,context))
            }catch{
                print(error)
                // TODO: - Log to Crashlytics
                debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error as NSError).userInfo)")
                completion(.failure(.readError(error)))
            }
        }
    }
    
    func retrieveRecords<M>(entityClass: M.Type,entityName: String,completion: @escaping ((Result<[M]?, CoreDataStorageError>)) -> Void) where  M: NSFetchRequestResult {
        //We need to create a context from this container
        coreData.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<M>(entityName: entityName)
            do {
                let result = try context.fetch(fetchRequest)
                completion(.success(result))
            } catch {
                // TODO: - Log to Crashlytics
                debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error as NSError).userInfo)")
                completion(.failure(.readError(error)))
            }
        }
    }
    
    func getRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping (Result<M?, CoreDataStorageError>) -> Void) where M: NSFetchRequestResult {
        
        coreData.performBackgroundTask { context in
            do {
                let fetchRequest = NSFetchRequest<M>(entityName: entityName)
                let key = keyValue.first?.key ?? ""
                let value = keyValue.first?.value
                let type = ((value as? String) != nil) ? "@" : "d"
                fetchRequest.predicate = NSPredicate(format: "\(key) == %\(type)", value as! CVarArg)
                let requestEntity = try context.fetch(fetchRequest)
                completion(.success(requestEntity.first))
            } catch {
                //TODO: Handle Error
                print(error.localizedDescription)
                // TODO: - Log to Crashlytics
                debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error as NSError).userInfo)")
                completion(.failure(.readError(error)))
            }
        }
    }
    
    func addChildToParentRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping (Results<M?, NSManagedObjectContext,CoreDataStorageError>) -> Void) where M: NSFetchRequestResult {
        
        coreData.performBackgroundTask { context in
            do {
                let fetchRequest = NSFetchRequest<M>(entityName: entityName)
                let key = keyValue.first?.key ?? ""
                let value = keyValue.first?.value
                let type = ((value as? String) != nil) ? "@" : "d"
                fetchRequest.predicate = NSPredicate(format: "\(key) == %\(type)", value as! CVarArg)
                let requestEntity = try context.fetch(fetchRequest)
                completion(.success(requestEntity.first,context))
            } catch {
                //TODO: Handle Error
                print(error.localizedDescription)
                // TODO: - Log to Crashlytics
                debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error as NSError).userInfo)")
                completion(.failure(.readError(error)))
            }
        }
    }
    
    func deleteRecordByKeyValue<M>(entityClass: M.Type,with entityName: String,keyValue: [String: Any],completion: @escaping ((Result<Bool?, CoreDataStorageError>)) -> Void) where M: NSManagedObject  {
        coreData.performBackgroundTask { context in
            do {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                let key = keyValue.first?.key ?? ""
                let value = keyValue.first?.value
                let type = ((value as? String) != nil) ? "@" : "d"
                fetchRequest.predicate = NSPredicate(format: "\(key) == %\(type)", value as! CVarArg)
                let requestEntity = try context.fetch(fetchRequest) as? [M]
                guard let firstObject = requestEntity?.first else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: StaticMessages.genericErrorMessage])
                    // TODO: - Log to Crashlytics
                    debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error).userInfo)")
                    return completion(.failure(.deleteError(error)))
                }
                context.delete(firstObject)
                try context.save()
                completion(.success(true))
            } catch {
                //TODO: Handle Error
                print(error.localizedDescription)
                // TODO: - Log to Crashlytics
                debugPrint("CoreDataMoviesResponseStorage Unresolved error \(error), \((error as NSError).userInfo)")
                completion(.failure(.readError(error)))
            }
        }
    }
     
    
    func deleteAllRecords(entityName: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteALL = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        contextExecute(by: entityName, deleteALL: deleteALL)
        contextSave()
    }
    
}


