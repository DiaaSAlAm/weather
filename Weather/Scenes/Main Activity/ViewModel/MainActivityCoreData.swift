//
//  MainActivityCoreData.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/25/21.
//

import Foundation

protocol MainActivityCoreDataProtocol {
    func deleteRecordById(id: Int)
    func retrieveRecords()
    func updateRecordById(id: Int,cityModel: MainCitiesModel?)
    var currentCitiesModel: Observable<[MainCitiesModel]> {get}
    var selectedCitiesModel: Observable<MainCitiesModel?> {get}
}

final class MainActivityCoreData: MainActivityCoreDataProtocol {
    
    var coreDataStorage: CoreDataStorageProtocol? = CoreDataStorage()
    private let entityName = "Cities"
    var currentCitiesModel: Observable<[MainCitiesModel]> = Observable([])
    var selectedCitiesModel: Observable<MainCitiesModel?> = Observable(nil)
    private var locationManger: LocationManagerProtocol = LocationManager()
    private var lat: Double?
    private var lon: Double?
    private var addressName: String?
    
    func deleteRecordById(id: Int){
        self.coreDataStorage?.deleteRecordByKeyValue(entityClass: Cities.self, with: self.entityName, keyValue: ["id":id], completion: { [weak self]  (result) in
            switch result {
            case .success(_):
                print("deleteRecordById success")
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
            }
        })
    }
    
    func retrieveRecords(){
        self.coreDataStorage?.retrieveRecords(entityClass: Cities.self, entityName: self.entityName, completion: {[weak self] (result) in
            switch result {
            case .success(let model):
                guard model?.count != 0 else {
                    self?.locationManger.enableLocation()
                    self?.observeCurrentLocation()
                    return
                }
                self?.currentCitiesModel.value =  model?.compactMap{$0.toDTO()} ?? []
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
            }
        })
        
    }
    
    func updateRecordById(id: Int,cityModel: MainCitiesModel?){
        self.coreDataStorage?.updateRecordByKeyValue(entityClass: Cities.self, entityName: self.entityName, keyValue: ["id":id], completion: { [weak self] (result) in
            switch result{
            case .success(let model,let context):
                guard let city = cityModel else {return}
                guard model != nil else {
                    self?.createNewRecords(cityModel: cityModel)
                    return
                }
                model?.toEntity(in: context, model: city)
                try? context.save()
                self?.getRecordById(id: id)
                return
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
                return
            }
        })
    }
    
    private func observeCurrentLocation(){
        locationManger.coordinate.observe(on: self) { [weak self] (coordinate) in
            self?.locationManger.city_countryName.observe(on: MainActivityViewModel.self) { (address) in
                self?.lat = coordinate?.latitude
                self?.lon = coordinate?.longitude
                self?.addressName = address
                guard self?.lat != nil , self?.lon != nil , self?.addressName != nil else {return}
                self?.updateRecordById(id: 0, cityModel: self?.insertCurrentLocation())
            }
        }
    }
    
    private func insertCurrentLocation() -> MainCitiesModel?{
        let model: MainCitiesModel? = MainCitiesModel(id: 0, city: self.addressName, lat: self.lat, lon: self.lon, dt: Date().timeIntervalSince1970, daily: [])
        return model
    }
    
    
    
    private func createNewRecords(cityModel: MainCitiesModel?){
        self.coreDataStorage?.createNewRecords(entityClass: Cities.self, entityName: self.entityName, completion: { [weak self] (result) in
            switch result{
            case .success(let model,let context):
                guard let city = cityModel else {return}
                model?.toEntity(in: context, model: city)
                try? context.save()
                self?.retrieveRecords()
                return
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
                return
            }
        })
    }
    
    private func getRecordById(id: Int){
        self.coreDataStorage?.getRecordByKeyValue(entityClass: Cities.self, with: self.entityName, keyValue: ["id":id], completion: { [weak self] (result) in
            switch result {
            case .success(let model):
                self?.selectedCitiesModel.value =  model?.toDTO()
            case .failure(let error):
                self?.showMessage(message: error.localizedDescription, messageKind: .error)
            }
        })
    }
    
    func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
}
