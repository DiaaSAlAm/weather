//
//  MainActivityViewModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/12/21.
//

import Foundation

final class MainActivityViewModel: MainActivityViewModelProtocol {
  
    //MARK: - Properties, OUTPUT
    var currentCitiesModel: Observable<[MainCitiesModel]> = Observable([])
    var selectedCitiesModel: Observable<MainCitiesModel?> = Observable(nil)
    var mainActivityCoreData: MainActivityCoreDataProtocol = MainActivityCoreData()
    
    //MARK: - Life Cycle, INPUT
    func viewDidLoad() {
        bindNewValues()
        mainActivityCoreData.retrieveRecords()
    }
    
   private func bindNewValues(){
        mainActivityCoreData.currentCitiesModel.observe(on: self) { [weak self] in self?.currentCitiesModel.value = $0}
        mainActivityCoreData.selectedCitiesModel.observe(on: self) { [weak self] in self?.selectedCitiesModel.value = $0}
        selectedCitiesModel.observe(on: self) { [weak self] (model)  in
            guard let self = self else {return}
            guard let id = model?.id else {return}
            self.chechIfListUpTo5()
            self.mainActivityCoreData.updateRecordById(id: id, cityModel: model)
        }
    }
    
    private func chechIfListUpTo5(){
        if currentCitiesModel.value.count == 5 {
            guard let id = currentCitiesModel.value.first?.id else {return}
            mainActivityCoreData.deleteRecordById(id: id)
        }
    }
    
    func configureCell(cell: CommonCellProtocol, indexPath: IndexPath) {
        guard let model = currentCitiesModel.value.getElement(at: indexPath.row) else {return}
        let viewModel = CurrentCitySetModel(city: model.city, icon: model.icon, tempMin: model.tempMin, tempMax: model.tempMax, lat: model.lat, lon: model.lon, dt: model.dt)
        cell.configureCell(setModel: viewModel)
    }
     
    func didDeleteItem(_ indexPath: IndexPath) {
        guard let model = currentCitiesModel.value.getElement(at: indexPath.row) else {return}
        currentCitiesModel.value.remove(at: indexPath.row)
        mainActivityCoreData.deleteRecordById(id: model.id)
    }
     
    func didSelectCurrentCity(_ indexPath: IndexPath) {
        guard let model = currentCitiesModel.value.getElement(at: indexPath.row) else {return}
        selectedCitiesModel.value = model
    }
}
