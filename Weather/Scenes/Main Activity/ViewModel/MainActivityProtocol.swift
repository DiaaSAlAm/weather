//
//  MainActivityViewModelProtocol.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/12/21.
//

import Foundation

protocol MainActivityViewModelInput {
    func viewDidLoad()
    func didDeleteItem(_ indexPath: IndexPath)
    func didSelectCurrentCity(_ indexPath: IndexPath)
    func configureCell(cell: CommonCellProtocol, indexPath: IndexPath)
}

protocol MainActivityViewModelOutput {
    var currentCitiesModel: Observable<[MainCitiesModel]> { get set }
    var selectedCitiesModel: Observable<MainCitiesModel?> { get } 
}
 

protocol MainActivityViewModelProtocol: MainActivityViewModelInput, MainActivityViewModelOutput {}

