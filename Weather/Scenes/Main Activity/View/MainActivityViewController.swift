//
//  MainActivityViewController.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import UIKit

class MainActivityViewController: UIViewController, StoryboardInstantiable {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    //MARK: - Properties
    var viewModel: MainActivityViewModelProtocol! = MainActivityViewModel()
    private let cellIdentifier = CellIdentifiers.commonCell 
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableView()
        bind(to: viewModel)
    }
    
    private func bind(to viewModel: MainActivityViewModelProtocol) {
        viewModel.viewDidLoad()
        viewModel.currentCitiesModel.observe(on: self) { [weak self]  _ in
            self?.tableView.reloadData()
        } 
    }
    
    //MARK: - Register  CollectionView Cell
    private func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    private func goToDaysForecast(){ 
        let view = DaysForecastViewController.create(with: viewModel.selectedCitiesModel)
        self.present(view, animated: true, completion: nil)
    }
}

// MARK: - UITableView Delegate
extension MainActivityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) } 
        viewModel.didSelectCurrentCity(indexPath)
        goToDaysForecast()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // disable remove first index ( if there is no more )
        guard viewModel.currentCitiesModel.value.count != 1 else { return UISwipeActionsConfiguration() }
        // delete action
        let delete = UIContextualAction(style: .normal,
                                        title: "Delete") { [weak self] (action, view, completionHandler) in
            self?.viewModel.didDeleteItem(indexPath)
            completionHandler(true)
        }
        delete.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [delete])
        
        return configuration
    }
}

// MARK: - UITableView Data Source
extension MainActivityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.currentCitiesModel.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CommonCell
        viewModel.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}
