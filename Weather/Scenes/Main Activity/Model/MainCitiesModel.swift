//
//  MainCitiesModel.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/8/21.
//

import Foundation

struct MainCitiesModel: Codable {
    var id: Int
    var city: String?
    var icon, weatherDescription: String?
    var tempMin, tempMax: Double?
    var lat, lon: Double?
    var dt: Double?
    var daily: [DaysForecastMappingModel]?
    
    init(id: Int, city: String? = nil,icon: String? = nil, weatherDescription: String? = nil,tempMin: Double? = nil, tempMax: Double? = nil, lat: Double? = nil, lon: Double? = nil, dt: Double? = nil, daily: [DaysForecastMappingModel]?) {
        self.id = id
        self.city = city
        self.icon = icon
        self.weatherDescription = weatherDescription
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.lat = lat
        self.lon = lon
        self.dt = dt
        self.daily = daily
    }
}

struct CurrentCitySetModel {
    var cityName: String?
    var celsius: String?
    var icon: String?
    var date: String?
    
    init(city: String? = nil,icon: String? = nil,tempMin: Double? = nil, tempMax: Double? = nil,lat: Double? = nil, lon: Double? = nil, dt: Double? = nil){
        self.cityName = city
        self.date = Date(timeIntervalSince1970: dt ?? 0).convertDateToString(dateFormat: Constants.dateFormatterMMM_d_h_mm_a)
        self.icon = Environment.imageURL.absoluteString + (icon ?? "") + Constants.image2xPng
        guard let min = tempMin , min != 0 else {return}
        guard let max = tempMax , max != 0 else {return}
        let celsiusMin = min.convertTemp(from: .kelvin, to: .celsius)
        let celsiusMax = max.convertTemp(from: .kelvin, to: .celsius)
        let fullCelsius = celsiusMax != "" ? "\(celsiusMin) / \(celsiusMax)" : celsiusMin
        self.celsius = fullCelsius
    }
}
