//
//  CitiesModelEntity+Mapping.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/17/21.
//

import Foundation
import CoreData

extension Cities {
    func toDTO() -> MainCitiesModel {
        let daily = daysForecast?.allObjects.map{($0 as! DaysForecast).toDTO()}
        let dailySorted = Array(daily?.sorted(by: { $0.dt < $1.dt }).prefix(5) ?? [])
        return .init(id: Int(id), city: city, icon: icon, weatherDescription: weatherDescription, tempMin: tempMin, tempMax: tempMax, lat: lat, lon: lon, dt: dt, daily: dailySorted )
    }
    
    func toEntity(in context: NSManagedObjectContext, model: MainCitiesModel) {
        id = Int64(model.id)
        city = model.city
        icon = model.icon
        weatherDescription = model.weatherDescription
        tempMin = model.tempMin ?? 0
        tempMax = model.tempMax ?? 0
        lat = model.lat ?? 0
        lon = model.lon ?? 0
        dt = model.dt ?? 0
        model.daily?.forEach {
            addToDaysForecast($0.toEntity(in: context))
        }
    }
}

extension MainCitiesModel {
    func toEntity(in context: NSManagedObjectContext) -> Cities {
        let entity: Cities = .init(context: context)
        entity.id = Int64(id)
        entity.city = city
        entity.icon = icon
        entity.weatherDescription = weatherDescription
        entity.tempMin = tempMin ?? 0
        entity.tempMax = tempMax ?? 0
        entity.lat = lat ?? 0
        entity.lon = lon ?? 0
        entity.dt = dt ?? 0
        daily?.forEach {
            entity.addToDaysForecast($0.toEntity(in: context))
        }
        return entity
    }
}
