//
//  CommonCell.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import UIKit

class CommonCell: UITableViewCell , CommonCellProtocol{

    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var leftLabel: UILabel!
    @IBOutlet private weak var middleLabel: UILabel!
    @IBOutlet private weak var iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(setModel: CurrentCitySetModel) {
        topLabel.text = setModel.date
        leftLabel.text = setModel.cityName
        middleLabel.text = setModel.celsius
        iconImage.loadImageAsync(with: setModel.icon)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
