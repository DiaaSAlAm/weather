//
//  MainNetwork.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/8/21.
//

import Foundation

enum MainNetwork {
    case getDaysForecast(_ lat: Double, _ lon: Double)
    case findCities(_ q: String)
}

extension MainNetwork: TargetType {
    
    var baseURL: String {
        return Environment.rootURL.absoluteString
    }
    
    var appId: String {
        return Environment.apiKey
    }
    
    var path: String {
        switch self {
        case .getDaysForecast:
            return "onecall"
        case .findCities:
            return "find"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return  [:]
        }
    }
    
    var task: Task {
        switch self {
        case .getDaysForecast(let lat,let lon):
            return .requestParametersURLEncoding(parameters: ["lat": lat, "lon": lon,"exclude": "hourly,minutely","appid" : appId])
        case .findCities(let q):
            return .requestParametersURLEncoding(parameters: ["q": q,"appid" : appId])
        }
    }
}
