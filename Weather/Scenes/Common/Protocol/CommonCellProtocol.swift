//
//  ProtocolCommonCell.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import Foundation

protocol CommonCellProtocol { // it's will call when register cell
    func configureCell(setModel: CurrentCitySetModel)
}
