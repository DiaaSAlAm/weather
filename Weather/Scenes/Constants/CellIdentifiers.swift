//
//  CellIdentifiers.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import Foundation

struct CellIdentifiers {
    static let commonCell = "CommonCell"
    static let daysForecastCell = "DaysForecastCell"
}
