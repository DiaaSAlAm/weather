//
//  Constants.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import Foundation

struct Constants {
    static let image2xPng = "@2x.png"
    static let latitude = 51.507351
    static let longitude = -0.127758
    static let dateFormatterMMM_d_h_mm_a = "MMM d, h:mm a"
    static let dateFormatterE_d_M = "E, d MMM"
}
