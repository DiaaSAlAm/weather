//
//  CoreDataEntityName.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/8/21.
//

import Foundation

struct CoreDataEntityName {
    static let search = "Search"
    static let cities = "Cities"
    
}
