//
//  StaticMessages.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import Foundation

struct StaticMessages {
    static let genericErrorMessage = "Oops! Looks like something went wrong. Offline mode is Active"
    static let notFound = "Not found. To make search more precise put the city's name, comma, 2-letter country code"
}
