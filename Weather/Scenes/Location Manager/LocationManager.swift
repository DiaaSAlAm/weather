//
//  LocationManager.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import UIKit
import CoreLocation

final class LocationManager: NSObject, CLLocationManagerDelegate ,LocationManagerProtocol {
    
    var viewController: UIViewController?
    private var locationManager = CLLocationManager()
    var coordinate: Observable<CLLocationCoordinate2D?> = Observable(nil)
    var city_countryName: Observable<String?> = Observable(nil)
    
    func enableLocation(){
        locationManager.delegate = self
        locationManager.startUpdatingLocation() 
        locationManager.requestWhenInUseAuthorization()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print(location.coordinate)
            coordinate.value = location.coordinate
            getCity_CountryFromLatLon(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) { [weak self] (address) in
                self?.city_countryName.value = address
            }
        }
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("in location change auth", status.rawValue)
        if status == .denied  || status == .restricted  {
            coordinate.value = CLLocationCoordinate2D(latitude: Constants.latitude, longitude: Constants.longitude)
            city_countryName.value = CitiesNames.defultCityLondonUK
        }
    }
    
    func getCity_CountryFromLatLon(latitude: Double, longitude: Double,completion: @escaping (String) -> Void) {
        let geoCoder = CLGeocoder()
        let locale = Locale(identifier: "en")
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, preferredLocale: locale, completionHandler: { (placemarks, error) -> Void in
            if (error != nil){
                print("reverse geodcode fail: \(error!.localizedDescription)")
                return
            }
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            var address = "" 
            if placeMark.locality != nil {
                address = address + placeMark.locality! + ", "
            }
            if placeMark.subAdministrativeArea != nil {
                address = address + placeMark.subAdministrativeArea!
            }
            completion(address)
        })
    }
}


