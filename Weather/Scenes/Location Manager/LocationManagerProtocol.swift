//
//  LocationManagerProtocol.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/25/21.
//

import Foundation
import CoreLocation

protocol  LocationManagerInput:class, CLLocationManagerDelegate {
    func enableLocation()
}

protocol LocationManagerOutput:class{
    var coordinate: Observable<CLLocationCoordinate2D?> { get }
    var city_countryName: Observable<String?> { get }
}

protocol LocationManagerProtocol:  LocationManagerInput, LocationManagerOutput {}
