//
//  StoryboardInstantiable.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import UIKit

public protocol StoryboardInstantiable: NSObjectProtocol {
    associatedtype T
    static var defaultFileName: String { get }
    static func instantiateViewController(_ storyboardName: String,_ bundle: Bundle?) -> T
}

public extension StoryboardInstantiable where Self: UIViewController {
    static var defaultFileName: String {
        return NSStringFromClass(Self.self).components(separatedBy: ".").last!
    }
    
    static func instantiateViewController(_ storyboardName: String,_ bundle: Bundle? = nil) -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "\(Self.self)") as? Self else {
            
            fatalError("Cannot instantiate initial view controller \(Self.self) from storyboard with name \(storyboardName)")
        }
        return vc
    }
}
