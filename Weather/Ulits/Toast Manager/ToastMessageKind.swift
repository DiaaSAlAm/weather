//
//  ToastMessageKind.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/14/21.
//

import Foundation

 enum ToastMessageKind {
     case info , error, success
 }
