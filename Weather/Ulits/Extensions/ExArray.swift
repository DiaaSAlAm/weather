//
//  ExArray.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
