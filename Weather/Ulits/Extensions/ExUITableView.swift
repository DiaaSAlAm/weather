//
//  ExUITableView.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import UIKit

extension UITableView {
    
    func fitHeightToContent(_ view: UIView,_ heightConstraint: NSLayoutConstraint,_ maxHeight: CGFloat) {
        self.reloadData()
        let contentSizeHeight = self.contentSize.height
        let height = contentSizeHeight > 0 ? contentSizeHeight + 24 : 0
        heightConstraint.constant = height > maxHeight ? maxHeight : height
        view.setNeedsLayout()
    }
    
}
