//
//  ExDate.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/7/21.
//

import Foundation

extension Date {
    
    func convertDateToString(dateFormat: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.string(from: self)
        return date
    } 
}

