//
//  SceneDelegate.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import UIKit
import CoreData

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    private(set) static var shared: SceneDelegate!

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) { 
        Self.shared = self
        self.window = self.window ?? UIWindow()
        AppStarter.shared.start()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
      self.saveContext()
    }
    
    

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
      // The persistent container for the application. This implementation
      // creates and returns a container, having loaded the store for the
      // application to it. This property is optional since there are legitimate
      // error conditions that could cause the creation of the store to fail.
      let container = NSPersistentContainer(name: "Weather")
      container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
          fatalError("Unresolved error \(error), \(error.userInfo)")
        }
      })
      return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
      let context = persistentContainer.viewContext
      if context.hasChanges {
        do {
          try context.save()
        } catch {
          let nserror = error as NSError
          fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
      }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }


}

