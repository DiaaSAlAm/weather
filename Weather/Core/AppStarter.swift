//
//  AppStarter.swift
//  Weather
//
//  Created by Diaa SAlAm on 7/5/21.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    private let window = (SceneDelegate.shared?.window)
    
    private init() {}
    
    func start() {
        AppTheme.apply()
        setRootViewController()
    }
    
    private func setRootViewController() {
        let view = MainActivityViewController.instantiateViewController(StoryboardNames.main)
        window?.rootViewController = view
        window?.makeKeyAndVisible()
    }
}
